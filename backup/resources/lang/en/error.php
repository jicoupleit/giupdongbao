<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Errors Language Lines
    |--------------------------------------------------------------------------
    |
    */

    "error_404"         => "404 Không tìm thấy trang",
    "error_403"         => "Lỗi 403",
    "error_406"         => "Lỗi 406",

    "error_404_description"         => "Rất tiếc! Không tìm thấy trang.",
    "error_404_subdescription"      => "Xin lỗi, trang đó không được tìm thấy!",
    "error_403_description"         => "Xin lỗi, truy cập bị cấm!",
    "error_406_description"         => "Xin lỗi, không thể chấp nhận được!",

    /*
    |--------------------------------------------------------------------------
    | Others Errors Language Lines
    |--------------------------------------------------------------------------
    |
    */

    "error_user_banned"         => "Xin lỗi, người dùng này đã bị cấm!",
    "error_user_delete"         => "Xin lỗi, người dùng này đã bị xóa!",
    "go_home"                   => "Tới trang chủ",

);
