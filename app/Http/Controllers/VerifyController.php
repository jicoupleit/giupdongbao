<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\CampaignsReported;
use App\Models\AdminSettings;
use App\Models\Verify;
use App\Models\Updates;
use App\Models\User;
use App\Helper;
use Carbon\Carbon;
use App\Models\Withdrawals;
use Image;
use Mail;

class VerifyController extends Controller
{

	public function __construct( AdminSettings $settings, Request $request) {
		$this->settings = $settings::first();
		$this->request = $request;
	}
	
	protected function validator(array $data, $id = null) {
	 	
    	Validator::extend('ascii_only', function($attribute, $value, $parameters){
    		return !preg_match('/[^x00-x7F\-]/i', $value);
		});
		
		Validator::extend('text_required', function($attribute, $value, $parameters)
			{
				$value = preg_replace("/\s|&nbsp;/",'',$value);   
			    return strip_tags($value);
			});
		
		$messages = array (
		'type.required' => "Type is required",
		'first_name.required' => "First Name is required",
		'last_name.required' => "First Name is required",
		'personal_id.required' => "Personal ID is required",
		//'ssn4.required' => "SSN Last 4 is required",
                'birthday.required' => "Birthday is required",
                'city.required' => "City is required",
                'country.required' => "Country is required",
                'line1.required' => "Line 1 is required",
                //'line2.required' => "Line 2 is required",
                'postal_code.required' => "Postal Code is required",
                'state.required' => "State is required",
                'ext_country.required' => "External Account is required",
                'ext_routing.required' => "External Account is required",
                'ext_account.required' => "External Account is required",
		'img.required' => "File upload is required",
		
		'b_country.required' => "Business Country is required",
		'b_zip.required' => "Business Postal Code is required",
		'b_state.required' => "Business State is required",
		'b_line1.required' => "Business Line 1 is required",
		//'b_line2.required' => "Business Line 2 is required",
		'b_city.required' => "Business City is required",
		'b_phone.required' => "Business Phone is required",

	);
		
		
		// Create Rules
		if( $id == null ) {
if(trim($this->request->type) == "individual"){
			return Validator::make($data, [
		'type'  => 'required',
        	'first_name'             => 'required|min:2|max:45',
                'last_name'             => 'required|min:2|max:45',
                'personal_id'             => 'required|min:3|max:20',
                //'ssn4'             => 'required|min:4|max:4',
                'birthday'             => 'required',
        	'city'  => 'required',
                'country'  => 'required',
                'line1'  => 'required',
                //'line2'  => 'required', 
                'postal_code'  => 'required',
                'state'  => 'required',
                'ext_country'  => 'required',
        	'ext_routing'        => 'required|max:50',
                'ext_account'        => 'required|max:50',
		'img'  => 'required',
		
		
            	        
        ], $messages);
		}else{
if(trim($this->request->type) == "partnership"){
return Validator::make($data, [
		'type'  => 'required',
        	'first_name'             => 'required|min:2|max:45',
                'last_name'             => 'required|min:2|max:45',
                'personal_id'             => 'required|min:3|max:20',
                //'ssn4'             => 'required|min:4|max:4',
                'birthday'             => 'required',
        	'city'  => 'required',
                'country'  => 'required',
                'line1'  => 'required',
                //'line2'  => 'required', 
                'postal_code'  => 'required',
                'state'  => 'required',
                'ext_country'  => 'required',
        	'ext_routing'        => 'required|max:50',
                'ext_account'        => 'required|max:50',
		'img'  => 'required',
		
		'b_country'  => 'required',
		'b_city'  => 'required',
		'b_line1'  => 'required',
		//'b_line2'  => 'required',
		'b_zip'  => 'required',
		'b_state'  => 'required',
		'b_phone'  => 'required',
            	        
        ], $messages);

}else{
return Validator::make($data, [
		'type'  => 'required',
        	'first_name'             => 'required|min:2|max:45',
                'last_name'             => 'required|min:2|max:45',
                'personal_id'             => 'required|min:3|max:20',
                //'ssn4'             => 'required|min:4|max:4',
                'birthday'             => 'required',
        	'city'  => 'required',
                'country'  => 'required',
                'line1'  => 'required',
                //'line2'  => 'required', 
                'postal_code'  => 'required',
                'state'  => 'required',
                'ext_country'  => 'required',
        	'ext_routing'        => 'required|max:50',
                'ext_account'        => 'required|max:50',
		'img'  => 'required',
		
		'b_country'  => 'required',
		'b_city'  => 'required',
		'b_line1'  => 'required',
		//'b_line2'  => 'required',
		'b_zip'  => 'required',
		'b_state'  => 'required',
		
            	        
        ], $messages);
}
}
		// Update Rules
		} else {
if(trim($this->request->type) == "individual"){
			return Validator::make($data, [
			'type'  => 'required',
		    	'first_name'             => 'required|min:2|max:45',
                        'last_name'             => 'required|min:2|max:45',
                        'personal_id'             => 'required|min:3|max:20',
                        //'ssn4'             => 'required|min:4|max:4',
                        'birthday'             => 'required',
                        'city'  => 'required',
                        'country'  => 'required',
                        'line1'  => 'required',
                        'line2'  => 'required', 
                        'postal_code'  => 'required',
                        'state'  => 'required',
                        'ext_country'  => 'required',
                        'ext_routing'        => 'required|max:50',
                        'ext_account'        => 'required|max:50',
			'img'  => 'required',

		        ], $messages);
}else{
if(trim($this->request->type) == "partnership"){
return Validator::make($data, [
			'type'  => 'required',
		    	'first_name'             => 'required|min:2|max:45',
                        'last_name'             => 'required|min:2|max:45',
                        'personal_id'             => 'required|min:3|max:20',
                        //'ssn4'             => 'required|min:4|max:4',
                        'birthday'             => 'required',
                        'city'  => 'required',
                        'country'  => 'required',
                        'line1'  => 'required',
                        'line2'  => 'required', 
                        'postal_code'  => 'required',
                        'state'  => 'required',
                        'ext_country'  => 'required',
                        'ext_routing'        => 'required|max:50',
                        'ext_account'        => 'required|max:50',
			'img'  => 'required',

			'b_country'  => 'required',
		'b_city'  => 'required',
		'b_line1'  => 'required',
		//'b_line2'  => 'required',
		'b_zip'  => 'required',
		'b_state'  => 'required',
		'b_phone'  => 'required',
], $messages);
}else{
return Validator::make($data, [
			'type'  => 'required',
		    	'first_name'             => 'required|min:2|max:45',
                        'last_name'             => 'required|min:2|max:45',
                        'personal_id'             => 'required|min:3|max:20',
                        //'ssn4'             => 'required|min:4|max:4',
                        'birthday'             => 'required',
                        'city'  => 'required',
                        'country'  => 'required',
                        'line1'  => 'required',
                        'line2'  => 'required', 
                        'postal_code'  => 'required',
                        'state'  => 'required',
                        'ext_country'  => 'required',
                        'ext_routing'        => 'required|max:50',
                        'ext_account'        => 'required|max:50',
			'img'  => 'required',

			'b_country'  => 'required',
		'b_city'  => 'required',
		'b_line1'  => 'required',
		//'b_line2'  => 'required',
		'b_zip'  => 'required',
		'b_state'  => 'required',
		
		        ], $messages);
}
}
		}
		
    }

	public function create() {
		
		$input      = $this->request->all();
		$validator = $this->validator($input);
		
		 if ($validator->fails()) {
	        return response()->json([
			        'success' => false,
			        'errors' => $validator->getMessageBag()->toArray(),
			    ]); 
	    	} //<-- Validator
		$curVerify =  Verify::where('user_id',Auth::user()->id)->first();
		$file = $this->request->file('img');
		$originalName = $file->getClientOriginalName();
		$date = $this->request->birthday;
		
		$arrDate = explode("-",$date);
		$day = $arrDate[0];
		$month = $arrDate[1];
		$year = $arrDate[2];
		
		if(isset($curVerify))
		{

			$allVerify = Verify::where('user_id',Auth::user()->id)->update(array(
										'type' => trim($this->request->type),
										'business_num' => trim($this->request->business_num),
										//'business_address' => trim($this->request->business_address),
										'business_name' => trim($this->request->business_name),
										'company_name' => trim($this->request->company_name),
										'phone' => trim($this->request->phone),
										'first_name' => trim($this->request->first_name),
										'last_name' => trim($this->request->last_name),
										'ext_country' => trim($this->request->ext_country),
										'ext_routing' => trim($this->request->ext_routing),
										'ext_account' => trim($this->request->ext_account),
										'city' => trim($this->request->city),
										'line1' => trim($this->request->line1),
										'line2' => trim($this->request->line2),
										'postal_code' => trim($this->request->postal_code),
										'state' => trim($this->request->state),
										'birthday' => trim($this->request->birthday),
										'ssn4' => trim($this->request->ssn4),
										'country' => trim($this->request->country),
										'b_phone' => trim($this->request->b_phone),
										'b_country' => trim($this->request->b_country),
										'b_city' => trim($this->request->b_city),
										'b_zip' => trim($this->request->b_zip),
										'b_state' => trim($this->request->b_state),
										'b_line1' => trim($this->request->b_line1),
										'b_line2' => trim($this->request->b_line2),	
										'img' => trim($originalName),

											
										));
		}
		else{
		\Stripe\Stripe::setApiKey($this->settings->stripe_secret_key);
		//Create account
try{
		
$uploadFile = \Stripe\FileUpload::create(
  array(
    "purpose" => "identity_document",
    "file" => fopen($file, 'r')
  )
);


$curAccount = \Stripe\Account::create(
    array(
        "country" => $this->request->country,
        "managed" => true,
	"email" => Auth::user()->email,
        "legal_entity" => array(
            'address' => array(
                'city' => $this->request->city,
                'country' => $this->request->country,
                "line1" => $this->request->line1,
                "line2" => $this->request->line2,
                "postal_code" => $this->request->postal_code,
                "state" => $this->request->state,
		
            ),
		'verification' => array(
                    //'status' => 'verified',
                    'document' => $uploadFile->id,
                    //'details' => null
                ),
		
	     "type" => $this->request->type,
            'business_name' => $this->request->business_name,
            'business_tax_id' => $this->request->business_num,
            'dob' => array(
                'day' => $day,
                'month' => $month,
                'year' => $year
            ),
            'first_name' => $this->request->first_name,
            'last_name' => $this->request->last_name,
            'personal_id_number' => trim($this->request->personal_id),
            //'ssn_last_4' => $this->request->ssn4,
            //'type' => 'sole_prop'
        ),
        'tos_acceptance' => array(
            'date' => time(),
            'ip' => $_SERVER['REMOTE_ADDR']
        ),
        'external_account' => array(
            "object" => "bank_account",
            "country" => $this->request->ext_country,
            "currency" => "usd",
            //"account_holder_name" => 'Jane Austen',
            //"account_holder_type" => 'individual',
            "routing_number" => $this->request->ext_routing,
            "account_number" => $this->request->ext_account
        )
    )
);
} catch(\Stripe\Error\Card $e) {
  // Since it's a decline, Stripe_CardError will be caught
  $body = $e->getJsonBody();
  $err  = $body['error'];

  print('Status is:' . $e->getHttpStatus() . "\n");
  print('Type is:' . $err['type'] . "\n");
  print('Code is:' . $err['code'] . "\n");
  // param is '' in this case
  //print('Param is:' . $err['param'] . "\n");
  //print('Message is:' . $err['message'] . "\n");
} catch (\Stripe\Error\InvalidRequest $e) {
  $body = $e->getJsonBody();
  $err  = $body['error'];
if($err['message']){
	$messages = array (
		'$err.message' => $err['message'],
	);

	return response()->json([
			        'success' => false,
			        'errors' => $messages,
			    ]); 

}

	//print('Message is:' . $err['message'] . "\n");

}

		//End create
                $sql                        = new Verify;
		$sql->user_id          = Auth::user()->id;
		$sql->first_name                = trim($this->request->first_name);
		$sql->last_name                = trim($this->request->last_name);
		$sql->ext_country                = trim($this->request->ext_country);
		$sql->ext_routing                = trim($this->request->ext_routing);
		$sql->ext_account                = trim($this->request->ext_account);
		$sql->country                = trim($this->request->country);
		$sql->city                = trim($this->request->city);
		$sql->line1                = trim($this->request->line1);
		$sql->line2                = trim($this->request->line2);	
		$sql->postal_code                = trim($this->request->postal_code);
		$sql->state                = trim($this->request->state);
		$sql->birthday                = trim($this->request->birthday);
		$sql->personal_id                = trim($this->request->personal_id);
		$sql->ssn4                = trim($this->request->ssn4);
		$sql->type                = trim($this->request->type);
		$sql->business_num                = trim($this->request->business_num);
		$sql->business_name                = trim($this->request->business_name);
		$sql->business_address                = trim($this->request->business_address);
		$sql->company_name                = trim($this->request->company_name);
		$sql->phone               = trim($this->request->phone);

		$sql->b_phone               = trim($this->request->b_phone);	
		$sql->b_country                = trim($this->request->b_country);
		$sql->b_city                = trim($this->request->b_city);
		$sql->b_line1                = trim($this->request->b_line1);
		$sql->b_line2                = trim($this->request->b_line2);	
		$sql->b_zip                = trim($this->request->b_zip);
		$sql->b_state                = trim($this->request->b_state);	
		
		$sql->img                 = trim($originalName);
		$sql->stripe_id                = $curAccount->id;
		$sql->save();
		}
		

		$_target = url('create/campaign');
		
		return response()->json([
				        'success' => true,
				        'target' => $_target,
				    ]);
		
	}//<<--- End Method

	
		
	
}
