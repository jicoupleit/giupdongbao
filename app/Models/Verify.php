<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verify extends Model {

	protected $guarded = array();
	public $timestamps = false;
}