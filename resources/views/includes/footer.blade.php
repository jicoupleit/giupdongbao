<!-- ***** Footer ***** -->
    <footer class="footer-main">
    	<div class="container">
    		
    		<div class="row">
    			<div class="col-md-3">
    				<a href="{{ url('/') }}">
    					<img src="{{ asset('public/img/watermark.png') }}" />
    				</a>
    			   <p class="margin-tp-xs">{{ $settings->description }}</p>
    			   

					 
    			</div><!-- ./End col-md-* -->

    			
    			
    			<div class="col-md-3 margin-tp-xs">
    				<h4 class="margin-top-zero">{{trans('misc.about')}}</h4>
    				<ul class="list-unstyled">
    					{{--@foreach( App\Models\Pages::all() as $page )--}}
        			{{--<li><a class="link-footer" href="{{ url('/page',$page->slug) }}">{{ $page->title }}</a></li>--}}
        	{{--@endforeach--}}
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/terms-and-conditions/">Terms and Conditions</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/dieu-khoan-va-dieu-kien/">Điều Khoản và Điều Kiện</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/privacy-policy/">Privacy Policy</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/chinh-sach-bao-mat/">Chính Sách Bảo Mật</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/le-phi/">Lệ Phí</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/cach-gay-quy/">Gây Quỹ</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/huong-dan/">Giúp Đỡ</a></li>
						{{--<li><a target="_blank" class="link-footer" href="http://giupdo.giupdongbao.com/giup-danh-cho-nha-tai-tro/">Giúp Đỡ – Dành cho Nhà Tài Trợ</a></li>--}}
						{{--<li><a target="_blank" class="link-footer" href="http://giupdo.giupdongbao.com/giup-danh-cho-nguoi-thu-huong/">Giúp Đở – Dành cho Người Thụ Hưởng</a></li>--}}
						{{--<li><a target="_blank" class="link-footer" href="http://giupdo.giupdongbao.com/giup-danh-cho-nguoi-tao-chien-dich/">Giúp Đỡ – Dành cho Người Tạo Chiến Dịch</a></li>--}}
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/lien-he/">Liên Hệ</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/ve-chung-toi/">Về chúng tôi</a></li>

    				</ul>
    			</div><!-- ./End col-md-* -->
    			
    			
    			<div class="col-md-3 margin-tp-xs">
    				<h4 class="margin-top-zero">{{trans('misc.categories')}}</h4>
    				<ul class="list-unstyled">
    					@foreach(  App\Models\Categories::where('mode','on')->orderBy('name')->take(6)->get() as $category )
        			<li><a class="link-footer" href="{{ url('category') }}/{{ $category->slug }}">{{ $category->name }}</a></li>
        	@endforeach
        	
        	@if( App\Models\Categories::count() > 6 )
        		<li><a class="link-footer" href="{{ url('categories') }}">
        			<strong>{{ trans('misc.view_all') }} <i class="fa fa-long-arrow-right"></i></strong>
        		</a></li>
        		@endif
        		
    				</ul>
    			</div><!-- ./End col-md-* -->
    			
    			<div class="col-md-3 margin-tp-xs">
    				<h4 class="margin-top-zero">Liên hệ</h4>
    				{{--<ul class="list-unstyled">--}}
        			{{----}}
        			{{--<li>--}}
        				{{--<a class="link-footer" href="{{ url('/') }}">{{ trans('misc.campaigns') }}</a>--}}
        			{{--</li>--}}
        			{{----}}
        			{{--@if( Auth::guest() )--}}
        			{{--<li>--}}
        				{{--<a class="link-footer" href="{{ url('login') }}">--}}
        					{{--{{ trans('auth.login') }}--}}
        				{{--</a>--}}
        				{{--</li>--}}

        				{{----}}
        				{{--@else--}}
        				{{--<li>--}}
	          		 		{{--<a href="{{ url('account') }}" class="link-footer">--}}
	          		 			{{--{{ trans('users.account_settings') }}--}}
	          		 		{{--</a>--}}
	          		 		{{--</li>--}}
	          		 		{{----}}
	          		 		{{--<li>--}}
	          		 			{{--<a href="{{ url('logout') }}" class="logout link-footer">--}}
	          		 				{{--{{ trans('users.logout') }}--}}
	          		 			{{--</a>--}}
	          		 		{{--</li>--}}
        				{{--@endif--}}
        				{{----}}
    				{{--</ul>--}}
					<div class="list-unstyled-lienhe">
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/gay-quy-cho-hoi-tu-thien/">Gây quỹ cho hội từ thiện của bạn</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdongbao.com/register">Đăng ký hội từ thiện của bạn</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdongbao.com/login">Tạo chiến dịch gây quỹ</a></li>
						<li><a target="_blank" class="link-footer" href="https://giupdo.giupdongbao.com/nhung-hoi-tu-thien-dang-ky-voi-chung-toi/">Danh sách hội từ thiện đăng ký với chúng tôi</a></li>
					</div>
						<ul class="list-inline">
						@if( $settings->twitter != '' )
							<li><a href="{{$settings->twitter}}" class="ico-social"><i class="fa fa-twitter"></i></a></li>
						@endif

						@if( $settings->facebook != '' )
							<li><a href="{{$settings->facebook}}" class="ico-social"><i class="fa fa-facebook"></i></a></li>
						@endif

						@if( $settings->instagram != '' )
							<li><a href="{{$settings->instagram}}" class="ico-social"><i class="fa fa-instagram"></i></a></li>
						@endif

						@if( $settings->linkedin != '' )
							<li><a href="{{$settings->linkedin}}" class="ico-social"><i class="fa fa-linkedin"></i></a></li>
						@endif

						@if( $settings->googleplus != '' )
							<li><a href="{{$settings->googleplus}}" class="ico-social"><i class="fa fa-google-plus"></i></a></li>
						@endif
					</ul >
    			</div><!-- ./End col-md-* -->
    		</div><!-- ./End Row -->
    	</div><!-- ./End Container -->
    </footer><!-- ***** Footer ***** -->

<footer class="subfooter">
	<div class="container">
	<div class="row">
    			<div class="col-md-12 text-center padding-top-20">
    				<p>&copy; {{ $settings->title }} - <?php echo date('Y'); ?></p>
    			</div><!-- ./End col-md-* -->
	</div>
</div>
</footer>    
