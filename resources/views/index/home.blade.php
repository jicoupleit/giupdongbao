<?php 
$settings = App\Models\AdminSettings::first(); 
$total_raised_funds = App\Models\Donations::sum('donation');
$total_campaigns    = App\Models\Campaigns::where('status','active')->count();
$total_members      = App\Models\User::count();
?>
@extends('app')

@section('content')
<div class="slider-home jumbotron index-header jumbotron_set jumbotron-cover @if( Auth::check() ) session-active-cover @endif">
      <div class="container wrap-jumbotron position-relative text-center bounceInLeft">
        <h1 class="title-site txt-center" id="titleSite">{{$settings->welcome_text}}</h1>
        <p class="subtitle-site txt-center"><strong>{{$settings->welcome_subtitle}}</strong></p>
		  <div class="action-re">
		  <a class="krown-button custom" href="https://giupdongbao.com/register" target="_self">Đăng Ký</a>
		  </div>
      </div><!-- container wrap-jumbotron -->
</div><!-- jumbotron -->

@if( $categories->count() != 0 )
<div class="container margin-bottom-40 header-home">
	
			<div class="col-md-12 btn-block margin-bottom-40 head-home">
				<h1 class="btn-block text-center class-montserrat margin-bottom-zero none-overflow">{{trans('misc.categories')}}</h1>
				{{--<h5 class="btn-block text-center class-montserrat subtitle-color">--}}
					{{--<a href="{{ url('categories') }}">--}}
			        			{{--<strong>{{ trans('misc.view_all') }} <i class="fa fa-long-arrow-right"></i></strong>--}}
			        		{{--</a>--}}
			        		{{--</h5>--}}
			</div>
			<div class="owl-carousel owl-theme">
			 @foreach(  App\Models\Categories::where('mode','on')->orderBy('name')->take(16)->get() as $category )
			     
			     @include('includes.categories-listing-all')
			     
			  @endforeach
			</div>

</div><!-- container -->
@endif

@if( $data->total() != 0 )
	<div class="container margin-bottom-40 header-home">
			<div class="col-md-12 btn-block margin-bottom-40">
				<h1 style="font-size: 30px;" class="btn-block text-center class-montserrat margin-bottom-zero none-overflow">{{trans('misc.campaigns')}}</h1>
				{{--<h5 class="btn-block text-center class-montserrat subtitle-color">{{trans('misc.recent_campaigns')}}</h5>--}}
			</div>			
		
		<div class="margin-bottom-30 campai">
			@include('includes.campaigns-home')
		</div>
		
		<div class="row margin-bottom-40" style="display: none;">
		
		<div class="container">
			<div class="col-md-4 border-stats">
					<h1 class="btn-block text-center class-montserrat margin-bottom-zero none-overflow"><span class=".numbers-with-commas counter"><?php echo html_entity_decode( App\Helper::formatNumbersStats($total_members) ) ?></span></h1>
					<h5 class="btn-block text-center class-montserrat subtitle-color text-uppercase">{{trans('misc.members')}}</h5>
				</div><!-- col-md-3 -->
				
			<div class="col-md-4 border-stats">
					<h1 class="btn-block text-center class-montserrat margin-bottom-zero none-overflow"><span class=".numbers-with-commas counter"><?php echo html_entity_decode( App\Helper::formatNumbersStats($total_campaigns) ) ?></span></h1>
					<h5 class="btn-block text-center class-montserrat subtitle-color text-uppercase">{{trans('misc.campaigns')}}</h5>
				</div><!-- col-md-3 -->
				
				<div class="col-md-4 border-stats">
					<h1 class="btn-block text-center class-montserrat margin-bottom-zero none-overflow">{{ $settings->currency_symbol }}<?php echo html_entity_decode( App\Helper::formatNumbersStats($total_raised_funds) ) ?></h1>
					<h5 class="btn-block text-center class-montserrat subtitle-color text-uppercase">{{trans('misc.funds_raised')}}</h5>
				</div><!-- col-md-3 -->

		</div><!-- row -->
		</div>
					
	</div><!-- container wrap-ui -->
	
	@else
	<div class="container margin-bottom-40">
		<div class="margin-bottom-30">
			<div class="btn-block text-center margin-top-40">
	    			<i class="ion ion-speakerphone ico-no-result"></i>
	    		</div>
	    		
	    		<h3 class="margin-top-none text-center no-result no-result-mg">
	    	{{ trans('misc.no_campaigns') }}
	    	</h3>
		</div>
	</div>
	@endif

	<div class="container">
		<h1 style="font-size:30px;" class="btn-block text-center class-montserrat margin-bottom-zero none-overflow">Chiến dịch nỗi bật</h1>
	</div>
@if( $campFeatured->total() != 0 )
	<div class="container margin-bottom-40 header-home">
	<div class="margin-bottom-30 campai">
			@include('includes.campFeatured-home')
		</div>
	</div>
@else
<div class="container margin-bottom-40">
		<div class="margin-bottom-30">
			<div class="btn-block text-center margin-top-40">
	    			<i class="ion ion-speakerphone ico-no-result"></i>
	    		</div>
	    		
	    		<h3 class="margin-top-none text-center no-result no-result-mg">
	    	{{ trans('misc.no_campaigns') }}
	    	</h3>
		</div>
	</div>

	
@endif
	<style type="text/css">
		.jumbotron.jumbotron-bottom.margin-bottom-zero.jumbotron-cover.infoter {
			background: #fff none repeat scroll 0 0;
		}
	</style>
	<div class="jumbotron jumbotron-bottom margin-bottom-zero jumbotron-cover infoter">
      <div class="container wrap-jumbotron position-relative">
        <h1 class="title-site footer">Gây quỷ với GiúpĐồngBào</h1>
        {{--<p class="subtitle-site txt-center"><strong>{{$settings->welcome_subtitle}}</strong></p>--}}
		  {{--<div class="images-footer">--}}
			  {{--<img src="http://giupdo.giupdongbao.com/wp-content/uploads/2017/04/001A_-newBANNER-GDB.png" alt="alt">--}}
		  {{--</div>--}}
		  <div class="col-lg-4">
			  <div class="box-footer text-center">
				  <div class="box-img">
				  	<img src="https://giupdo.giupdongbao.com/wp-content/uploads/2017/04/img1.png" alt="alt">
				  </div>
				  <h3>Gây quỷ cho mọi trường hợp</h3>
				  <p>Hệ thống giúp đồng bào gây quỹ cho mọi trường hợp, hệ thống dễ sử dụng, rất đơn giản. Không giới hạn thời gian hoặc số tiền của mục đích</p>
			  </div>
		  </div>
		  <div class="col-lg-4">
			  <div class="box-footer text-center">
				  <div class="box-img">
				  	<img src="https://giupdo.giupdongbao.com/wp-content/uploads/2017/04/img2.png" alt="alt">
				  </div>
				  <h3>Giúp đỡ tận tâm</h3>
				  <p>
					  Nhóm hỗ trợ khách hàng của chúng tôi sẵn sàng trợ giúp tất cả các câu hỏi hoặc thắc mắc của bạn
				  </p>
			  </div>
		  </div>
		  <div class="col-lg-4">
			  <div class="box-footer text-center">
				  <div class="box-img">
				  	<img src="https://giupdo.giupdongbao.com/wp-content/uploads/2017/04/img3.png" alt="alt">
				  </div>
				  <h3>Chuyển khoản trực </h3>
				  <p>
					  Mọi nguyễn góp được chuyển khoản trực tiếp đến tài khoản của chiến dịch chứ không qua tài khoản ngân hàng của GiúpĐồngBào
				  </p>
			  </div>
		  </div>
		  <style>
			  .images-footer > img {
				  width: 100%;
			  }
		  </style>
		  <div class="action-re">
			  <a class="krown-button custom" href="https://giupdongbao.com/register" target="_self">Đăng Ký</a>
		  </div>

      </div><!-- container wrap-jumbotron -->
    </div><!-- jumbotron -->
@endsection

@section('javascript')
	<script src="{{ asset('public/plugins/jquery.counterup/waypoints.min.js') }}"></script>
	<script src="{{ asset('public/plugins/jquery.counterup/jquery.counterup.min.js') }}"></script>
	
		<script type="text/javascript">
		
		$(document).on('click','#campaigns .loadPaginator', function(r){
			r.preventDefault();
			 $(this).remove();
			 $('.loadMoreSpin').remove();
					$('<div class="col-xs-12 loadMoreSpin"><a class="list-group-item text-center"><i class="fa fa-circle-o-notch fa-spin fa-1x fa-fw"></i></a></div>').appendTo( "#campaigns" );
					
					var page = $(this).attr('href').split('page=')[1];
					$.ajax({
						url: '{{ url("ajax/campaigns") }}?page=' + page
					}).done(function(data){
						if( data ) {
							$('.loadMoreSpin').remove();
							
							$( data ).appendTo( "#campaigns" );
						} else {
							bootbox.alert( "{{trans('misc.error')}}" );
						}
						//<**** - Tooltip
					});
			});
	
		jQuery(document).ready(function( $ ) {
			$('.counter').counterUp({
			delay: 10, // the delay time in ms
			time: 1000 // the speed time in ms
			});
		});
		
		 @if (session('success_verify'))
    		swal({   
    			title: "{{ trans('misc.welcome') }}",   
    			text: "{{ trans('users.account_validated') }}",   
    			type: "success",   
    			confirmButtonText: "{{ trans('users.ok') }}" 
    			});
   		 @endif
   		 
   		 @if (session('error_verify'))
    		swal({   
    			title: "{{ trans('misc.error_oops') }}",   
    			text: "{{ trans('users.code_not_valid') }}",   
    			type: "error",   
    			confirmButtonText: "{{ trans('users.ok') }}" 
    			});
   		 @endif
		
		</script>

@endsection