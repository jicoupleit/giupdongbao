@extends('app')

@section('title'){{'History - ' }}@endsection

@section('css')
<!-- Current locale and alternate locales -->
<meta property="og:locale" content="en_US" />
<meta property="og:locale:alternate" content="es_ES" />

@endsection

@section('content')

<div class="jumbotron md header-donation jumbotron_set">
      <div class="container wrap-jumbotron position-relative">
      	<h2 class="title-site">History</h2>
      	<p class="subtitle-site"><strong>{{ trans('misc.donate') }}</strong></p>
      </div>
    </div>
<div class="container margin-bottom-40">
	<div class="col-md-8 margin-bottom-20">
		<ul class="list-group" id="listDonations">
			<li class="list-group-item"><i class="fa fa-clock-o myicon-right"></i> <strong>{{trans('misc.recent_donations')}}</strong> </li>

			@foreach( $donations as $donation )

				<?php
				$letter = str_slug(mb_substr( $donation->fullname, 0, 1,'UTF8'));

				if( $letter == '' ) {
					$letter = 'N/A';
				}

				if( $donation->anonymous == 1 ) {
					$letter = 'N/A';
					$donation->fullname = trans('misc.anonymous');
				}
				?>

				@include('includes.listing-donations')

			@endforeach

			{{ $donations->links('vendor.pagination.loadmore') }}

		</ul>
	</div>
	<div class="col-md-4">
		@include('users.navbar-edit')
	</div>
</div>

	
	
@endsection

@php session()->forget('donation_cancel') @endphp
@php session()->forget('donation_success') @endphp