<?php 
// ** Data User logged ** //
     $user = Auth::user();
	 $settings = App\Models\AdminSettings::first();	
	$curVerify = App\Models\Verify::where('user_id',Auth::user()->id)->first(); 	 	 
	  ?>
@extends('app')

@section('title') {{ trans('misc.withdrawals') }} {{ trans('misc.configure') }} - @endsection
@section('css')
<link rel="stylesheet" href="{{ asset('public/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content') 
<div class="jumbotron md header-donation jumbotron_set">
      <div class="container wrap-jumbotron position-relative">
        <h2 class="title-site">{{ trans('misc.withdrawals') }} {{ trans('misc.configure') }} </h2>
      </div>
    </div>

<div class="container margin-bottom-40">
	
		<!-- Col MD -->
<div class="col-md-8 margin-bottom-20">
	
	@if (session('error'))
			<div class="alert alert-danger btn-sm alert-fonts" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            		{{ session('error') }}
            		</div>
            	@endif
            	
            	@if (session('success'))
			<div class="alert alert-success btn-sm alert-fonts" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            		{{ session('success') }}
            		</div>
            	@endif
            	
           @include('errors.errors-forms')

<h5>{{ trans('misc.select_method_payment') }} - <strong>{{ trans('misc.default_withdrawal') }}</strong>: @if( Auth::user()->payment_gateway == '' ) {{trans('misc.unconfigured')}} @else {{Auth::user()->payment_gateway}} @endif</h5>

	<a class="btn btn-primary pp btn-block" role="button" data-toggle="collapse" href="#paypal" aria-expanded="false" aria-controls="paypal">
	  <i class="fa fa-paypal myicon-right"></i> Paypal
	</a>
	
	<!-- collapse -->
	<div class="collapse margin-top-15" id="paypal">
	<form method="post" action="{{url('withdrawals/configure/paypal')}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	  <div class="form-group">
	    <input type="text" class="form-control" value="{{Auth::user()->paypal_account}}" id="email_paypal" name="email_paypal" placeholder="Email Paypal">
	  </div>
	  
	  <div class="form-group">
	    <input type="text" class="form-control" name="email_paypal_confirmation" placeholder="{{trans('misc.confirm_email')}}">
	  </div>
	  
	  <button type="submit" class="btn btn-success">{{ trans('misc.submit') }}</button>
	</form>
	</div><!-- collapse -->

	<button class="btn btn-default bank margin-top-10 btn-block" type="button" data-toggle="collapse" data-target="#bank" aria-expanded="false" aria-controls="bank">
	  <i class="fa fa-university myicon-right"></i> {{ trans('misc.bank_transfer') }} 
	</button>

   

	<!-- collapse -->
	<div class="collapse margin-top-15" id="bank">
	<form method="post" action="{{url('withdrawals/configure/bank')}}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	  <div class="form-group">
	    <textarea class="form-control" rows="5" id="bank_form" name="bank" placeholder="{{ trans('misc.bank_details') }}">{{Auth::user()->bank}}</textarea>
	  </div>
	  <button type="submit" class="btn btn-success">{{ trans('misc.submit') }}</button>
	</form>
	</div><!-- collapse -->
<h3>Stripe Infomation</h3>
<!-- form start -->
    <form method="POST" action="{{ url('create/campaign/verify') }}" enctype="multipart/form-data" id="formUpload">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    		<div class="col-xs-12"><h4>Account Details</h4>
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Your business type</label>
                      	<select name="type" class="form-control typeAccount">
                      		<option value="">Select</option>
                            	<option @if($curVerify)@if($curVerify->type =='individual') selected @endif @endif value="individual">Individual</option>
				<option @if($curVerify)@if($curVerify->type =='partnership') selected @endif @endif value="partnership">Partnership</option>
				<option @if($curVerify)@if($curVerify->type =='company') selected @endif @endif value="company">Corporation</option>
				
			</select>
                  </div><!-- /.form-group-->                    
		

		<!-- Start Form Group -->
                    <div class="form-group col-xs-5">
                      <label>Upload File</label>
			
			<input type="file" class="form-control uploadVerify" name="img" id="img"/>
                        
                    </div><!-- /.form-group-->

		@if($curVerify)		
		<!-- Start Form Group -->
                    <div class="form-group col-xs-3">
                      <label>Uploaded File</label>
			
			<p>{{ $curVerify->img }}</p>                        
                    </div><!-- /.form-group-->
		@endif

		<!-- Start Form Group -->
                    <div class="form-group col-xs-4 phonePar">
                      <label>Phone</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->b_phone }}@endif" name="b_phone" id="b_phone" class="form-control" placeholder="555-678-1212">
                    </div><!-- /.form-group-->
		</div>

		<div class="col-xs-12"><h4>Name</h4>
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>First Name</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->first_name }}@endif" name="first_name" id="first_name" class="form-control" placeholder="First Name">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Last Name</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->last_name }}@endif" name="last_name" id="last_name" class="form-control" placeholder="Last Name">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Personal ID</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->personal_id }}@endif" name="personal_id" id="personal_id" class="form-control" placeholder="Personal ID">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>SSN Last 4</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->ssn4 }}@endif" name="ssn4" id="ssn4" class="form-control" placeholder="SSN Last 4">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>DOB</label>
                      <div class="input-group">
                      	<div class="input-group-addon addon-dollar"><i class="fa fa-calendar"></i></div>
                        <input type="text" value="@if($curVerify){{ $curVerify->birthday }}@endif" id="datepicker_birthday" name="birthday" class="form-control" placeholder="DOB">
                      </div>
                      
                      
                 </div><!-- /.form-group-->

		</div>
		<div class="col-xs-12"><h4>Address</h4>		
                 
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Address Line1</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->line1 }}@endif" name="line1" id="line1" class="form-control" placeholder="line1">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Address Line2</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->line2 }}@endif" name="line2" id="line2" class="form-control" placeholder="line2">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>City</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->city }}@endif" name="city" id="city" class="form-control" placeholder="City">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>State</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->state }}@endif" name="state" id="state" class="form-control" placeholder="state">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Postal/Zip</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->postal_code }}@endif" name="postal_code" id="postal_code" class="form-control" placeholder="Postal Code">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Country</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->country }}@endif" name="country" id="country" class="form-control" placeholder="Country">
                    </div><!-- /.form-group-->
		
		
		
		
		
		
		</div>
                 <div class="col-xs-12"><h4>Payment Information</h4>   
                 <!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Country Code</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->ext_country }}@endif" name="ext_country" id="ext_country" class="form-control" placeholder="Character country code">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Routing Number</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->ext_routing }}@endif" name="ext_routing" id="ext_routing" class="form-control" placeholder="Routing Number">
                    </div><!-- /.form-group-->                     
                 <!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Account Number</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->ext_account }}@endif" name="ext_account" id="ext_account" class="form-control" placeholder="Account Number">
                    </div><!-- /.form-group-->                     
                  </div>                     
		<div class="col-xs-12 business" style="display:none;">

			<h4>Business Information</h4>   
                 <div class="form-group col-xs-4">
                      <label>Business number(Tax ID)</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->business_num }}@endif" name="business_num" id="business_num" class="form-control" placeholder="123456789">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4 company_name">
                      <label>Legal name</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->company_name }}@endif" name="company_name" id="company_name" class="form-control" placeholder="Company, Inc.">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Business name</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->business_name }}@endif" name="business_name" id="business_name" class="form-control" placeholder="mycompany.com">
                    </div><!-- /.form-group-->
			
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Phone</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->phone }}@endif" name="phone" id="phone" class="form-control" placeholder="555-678-1212">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Address Line1</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->b_line1 }}@endif" name="b_line1" id="b_line1" class="form-control" placeholder="line1">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Address Line2</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->b_line2 }}@endif" name="b_line2" id="b_line2" class="form-control" placeholder="line2">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>City</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->b_city }}@endif" name="b_city" id="b_city" class="form-control" placeholder="City">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>State</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->b_state }}@endif" name="b_state" id="b_state" class="form-control" placeholder="state">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Postal/Zip</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->b_zip }}@endif" name="b_zip" id="b_zip" class="form-control" placeholder="Postal Code">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-4">
                      <label>Country</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->b_country }}@endif" name="b_country" id="b_country" class="form-control" placeholder="Country">
                    </div><!-- /.form-group-->
		
		
		
		
		
		</div>
		
				
                    <!-- Alert -->
                    <div class="alert alert-danger display-none" style="margin-top: 80px;" id="dangerAlert">
			<ul class="list-unstyled" id="showErrors"></ul>
		    </div><!-- Alert -->
                
                  <div class="box-footer">
                  	<hr />
		@if($curVerify)	
                    	<button type="submit" id="buttonFormSubmit" class="btn btn-block btn-lg btn-main custom-rounded">Update Account</button>
			<a href="{{ url('create/campaign') }}" id="" class="btn btn-block btn-lg btn-main custom-rounded">Create Campaign</a>
		@else    
			<button type="submit" id="buttonFormSubmit" class="btn btn-block btn-lg btn-main custom-rounded">Create Account</button>
		@endif
                  </div><!-- /.box-footer -->
                </form>



	  		 			
</div><!-- /COL MD -->
		
		<div class="col-md-4">
			@include('users.navbar-edit')
		</div>
		
 </div><!-- container -->
 
 <!-- container wrap-ui -->
@endsection

@section('javascript')
<script src="{{ asset('public/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	//Date picker
    $('#datepicker, #datepicker_birthday').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      maxDate: "+0D",
      language: 'en'
    });

	

	$(document).on('click','.pp',function(s){
		$('#bank').collapse('hide');
		$('#email_paypal').focus();
	});
	
	$(document).on('click','.bank',function(s){
		$('#paypal').collapse('hide');
		$('#bank_form').focus();
	});
	$(document).ready(function() {
      	var type = $('select').val();
		if(type=='company' || type=='partnership'){
			$('.business').show();
			
}
		else
			$('.business').hide();

});
	$(document).on('change','.typeAccount',function(s){
		var type = $('select').val();
		if(type=='company' || type=='partnership'){
			$('.business').show();
			}
		else
			$('.business').hide();
	});

	
</script>
@endsection