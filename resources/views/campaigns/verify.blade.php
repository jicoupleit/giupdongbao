<?php 
	$settings = App\Models\AdminSettings::first(); 
	$curVerify = App\Models\Verify::where('user_id',Auth::user()->id)->first();
?>
@extends('app')

@section('title'){{ trans('misc.create_campaign').' - ' }}@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('public/plugins/datepicker/datepicker3.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
 
 <div class="jumbotron md index-header jumbotron_set jumbotron-cover">
      <div class="container wrap-jumbotron position-relative">
      	<h2 class="title-site">Create Stripe Connect Account</h2>
      </div>
    </div>
  
<div class="container margin-bottom-40 padding-top-40">
	<div class="row">
		
	<!-- col-md-8 -->
	<div class="col-md-12">
		<div class="wrap-center center-block">
			@include('errors.errors-forms')

@if( Auth::user()->status == 'active' )			
    <!-- form start -->
    <form method="POST" action="{{ url('create/campaign/verify') }}" enctype="multipart/form-data" id="formUpload">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
    		<h4>Name</h4>
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>First Name</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->first_name }}@endif" name="first_name" id="first_name" class="form-control" placeholder="First Name">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Last Name</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->last_name }}@endif" name="last_name" id="last_name" class="form-control" placeholder="Last Name">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Personal ID</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->personal_id }}@endif" name="personal_id" id="personal_id" class="form-control" placeholder="Personal ID">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>SSN Last 4</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->ssn4 }}@endif" name="ssn4" id="ssn4" class="form-control" placeholder="SSN Last 4">
                    </div><!-- /.form-group-->
		<h4>DOB</h4>
		<!-- Start Form Group -->
                    <div class="form-group">
                      <label>DOB</label>
                      <div class="input-group">
                      	<div class="input-group-addon addon-dollar"><i class="fa fa-calendar"></i></div>
                        <input type="text" value="@if($curVerify){{ $curVerify->birthday }}@endif" id="datepicker_birthday" name="birthday" class="form-control" placeholder="DOB">
                      </div>
                      
                      
                 </div><!-- /.form-group-->

		
		<h4>Address</h4>		
                 <!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>City</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->city }}@endif" name="city" id="city" class="form-control" placeholder="City">
                    </div><!-- /.form-group-->

		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Country</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->country }}@endif" name="country" id="country" class="form-control" placeholder="Country">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Line1</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->line1 }}@endif" name="line1" id="line1" class="form-control" placeholder="line1">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Line2</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->line2 }}@endif" name="line2" id="line2" class="form-control" placeholder="line2">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Postal Code</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->postal_code }}@endif" name="postal_code" id="postal_code" class="form-control" placeholder="Postal Code">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>State</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->state }}@endif" name="state" id="state" class="form-control" placeholder="state">
                    </div><!-- /.form-group-->
                 <h4>External Account</h4>   
                 <!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Country</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->ext_country }}@endif" name="ext_country" id="ext_country" class="form-control" placeholder="ext_country">
                    </div><!-- /.form-group-->
		<!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Routing Number</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->ext_routing }}@endif" name="ext_routing" id="ext_routing" class="form-control" placeholder="Routing Number">
                    </div><!-- /.form-group-->                     
                 <!-- Start Form Group -->
                    <div class="form-group col-xs-6">
                      <label>Account Number</label>
                        <input type="text" value="@if($curVerify){{ $curVerify->ext_account }}@endif" name="ext_account" id="ext_account" class="form-control" placeholder="Account Number">
                    </div><!-- /.form-group-->                     
                    

                  
                                     

                    <!-- Alert -->
                    <div class="alert alert-danger display-none" id="dangerAlert">
							<ul class="list-unstyled" id="showErrors"></ul>
						</div><!-- Alert -->
                
                  <div class="box-footer">
                  	<hr />
		@if($curVerify)	
                    	<button type="submit" id="buttonFormSubmit" class="btn btn-block btn-lg btn-main custom-rounded">Update Account</button>
			<a href="{{ url('create/campaign') }}" id="" class="btn btn-block btn-lg btn-main custom-rounded">Create Campaign</a>
		@else    
			<button type="submit" id="buttonFormSubmit" class="btn btn-block btn-lg btn-main custom-rounded">Create Account</button>
		@endif
                  </div><!-- /.box-footer -->
                </form>
                
                @else
                
	<div class="btn-block text-center margin-top-40">
	    			<i class="icon-warning ico-no-result"></i>
	    		</div>
	    		
	   <h3 class="margin-top-none text-center no-result no-result-mg">
	    	{{trans('misc.confirm_email')}} <strong>{{Auth::user()->email}}</strong>
	    	</h3>
                
                @endif
               
               </div><!-- wrap-center -->
		</div><!-- col-md-12-->
				
	</div><!-- row -->
</div><!-- container -->

@endsection

@section('javascript')
	<script src="{{ asset('public/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('public/plugins/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('public/plugins/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	//Date picker
    $('#datepicker, #datepicker_birthday').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      language: 'en'
    });
    
    $(document).ready(function() {
	
    $("#onlyNumber").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    $('input').iCheck({
	  	checkboxClass: 'icheckbox_square-red',
    	radioClass: 'iradio_square-red',
	    increaseArea: '20%' // optional
	  });
	  
});

	//Flat red color scheme for iCheck
        $('input[type="radio"]').iCheck({
          radioClass: 'iradio_flat-red'
        });	 	
</script>
@endsection