<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'email_sent' 			  => 	  'Email với các thiết lập lại mật khẩu đã được gửi đi.',
    'reset_password'		  =>	  'Thiết lập lại mật khẩu',
    'enter_email_pass'		  =>	  'Nhập email của bạn và mật khẩu mới',
    'email_has_been_set'	  =>	  'Gửi thành công, hãy kiểm tra lại email của bạn',
    'or_sign_in_with'		  =>	  'Hoặc Đăng nhập với ...',
    'or_sign_up_with'		  =>	  'Hoặc Đăng ký với ...',
    'username_or_email'		  =>	  'Tài khoản hoặc Email',
    'username'		          =>	  'Tài khoản',
    'full_name'		          =>	  'Họ và tên',
    'team_name'		          =>	  'Tên team',
    'status'		          =>	  'Trạng thái',
    'login'				      =>	  'Đăng Nhập',
    'forgot_password'		  =>	  'Quên mật khẩu?',
    'confirm_password'		  =>	  'Xác nhận mật khẩu',
    'sign_up'				  =>	  'Đăng Ký',
    'sign_in'		          =>	  'Đăng Nhập',
    'remember_me'		      =>	  'Ghi nhớ tôi',
    'logout'				  =>	  'Đăng Xuất',
    'back'				      =>	  'Quay trở lại',
    'password_reset'	 	  =>	  'Mật khẩu của bạn đã được đặt lại thành công.',
    'password_recover'        =>      'Khôi phục mật khẩu',
    'password_reset_2'        =>      'Đặt lại mật khẩu',
    'email'                   =>      'Email',
    'password'                =>      'Mật khẩu',
    'password_confirmation'   =>      'Xác nhận mật khẩu',
    'send'                    =>      'Gửi',
    'password_reset_mail'     =>      'Để thiết lập lại mật khẩu của bạn, điền vào mẫu này:',
    'login_required'          =>      'Bạn phải đăng nhập để tiếp tục',
    'check_account'           =>      'Xin chúc mừng! kiểm tra email của bạn để kích hoạt tài khoản của bạn, nếu bạn không thấy email kiểm tra thư mục Spam của bạn',
    'success_update'          =>      'Tài khoản của bạn đã được cập nhật thành công',
    'success_update_password' =>      'Mật khẩu của bạn đã được cập nhật thành công',
    'login'                   =>      'Đăng Nhập',
    'not_have_account'        =>      "Không có tài khoản?",
    'already_have_an_account' =>      'Bạn co sẵn sàng để tạo một tai khoản?',
    'terms'                   =>      'Khi bạn nhấn vào đây để Đăng ký bạn đã đồng ý với Điều khoản và Điều kiện của chúng tôi',
    'logged_in_comments'      =>      'Bạn phải đăng nhập để bình luận bức ảnh này',
    'error_captcha'           =>      'Captcha lỗi',
    'pay_register'            =>      'Trả tiền và đăng ký',
    'success_account'         =>      'Tài khoản của bạn đã được tạo thành công, xin vui lòng đăng nhập',
    'error_desc'              =>      'Lỗi chính! Đã có một số vấn đề với đầu vào của bạn.',
    'error_logging'           =>      'Những thông tin không phù hợp với hồ sơ của chúng tôi.',
);